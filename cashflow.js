/** @OnlyCurrentDoc */

  function SaveCashFlow() {
    var spreadsheet = SpreadsheetApp.getActive();
    spreadsheet.getRange('24:24').activate();
    spreadsheet.getRange('A7:I7').activate();
    spreadsheet.setActiveSheet(spreadsheet.getSheetByName('היסטוריה'), true);
    spreadsheet.getRange('20:20').activate();
    spreadsheet.getActiveSheet().insertRowsAfter(spreadsheet.getActiveRange().getRow(), 1);
    spreadsheet.getActiveRange().offset(0, 0, 1, spreadsheet.getActiveRange().getNumColumns()).activate();
    spreadsheet.getActiveRangeList().setBackground('#ffffff');
    spreadsheet.getRange('A21').activate();
    spreadsheet.getRange('\'ניהול\'!A7:I7').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_NORMAL, false);
    spreadsheet.setActiveSheet(spreadsheet.getSheetByName('הגדרות'), true);
    spreadsheet.getRange('A21:I21').activate();
    spreadsheet.getRange('L4:M4').activate();
    spreadsheet.setActiveSheet(spreadsheet.getSheetByName('היסטוריה'), true);
    spreadsheet.getRange('J21').activate();
    spreadsheet.getRange('\'הגדרות\'!L4:M4').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_NORMAL, false);
    spreadsheet.setActiveSheet(spreadsheet.getSheetByName('ניהול'), true);
  };