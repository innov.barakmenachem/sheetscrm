function InterestingOtherCall() {
  var spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.getRange('A27:J34').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Call Template'), true);
  spreadsheet.getRange('A27:J39').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
  spreadsheet.getRange('A35').activate();
  spreadsheet.getRange('\'Call Template\'!A27:J39').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_NORMAL, false);
};

function sellFlutterEXPERT() {
var spreadsheet = SpreadsheetApp.getActive();
spreadsheet.getRange('A35:J36').activate();
spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Call Template'), true);
spreadsheet.getRange('A44:E46').activate();
spreadsheet.getCurrentCell().setValue('Flutter EXPERT');
spreadsheet.getRange('F44:J46').activate();
spreadsheet.getCurrentCell().setValue('550 במקום 1450 - הנחת סטודנטים + הנחת סגר - עד 12 תשלומים');
spreadsheet.getRange('A41:J53').activate();
spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
spreadsheet.getRange('\'Call Template\'!A41:J53').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_NORMAL, false);
};

function sellFlutterZERO() {
var spreadsheet = SpreadsheetApp.getActive();
spreadsheet.getRange('A27:J34').activate();
spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Call Template'), true);
spreadsheet.getRange('A44:E46').activate();
spreadsheet.getCurrentCell().setValue('Flutter From Zero to Hero');
spreadsheet.getRange('F44:J46').activate();
spreadsheet.getCurrentCell().setValue('לייפטיים - רגיל - 550 שקלים (עד 6 תשלומים)');
spreadsheet.getRange('A41:J53').activate();
spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
spreadsheet.getRange('A35:J49').activate();
spreadsheet.getRange('\'Call Template\'!A41:J53').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_NORMAL, false);
};

function loadLid() {
var spreadsheet = SpreadsheetApp.getActive();
spreadsheet.getRange('C10').activate();
};




function saveCall() {
var spreadsheet = SpreadsheetApp.getActive();
spreadsheet.getRange('A6:J98').activate();
spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Lids History'), true);
spreadsheet.getRange('1:100').activate();
spreadsheet.getActiveSheet().insertRowsBefore(spreadsheet.getActiveRange().getRow(), 100);
spreadsheet.getActiveRange().offset(0, 0, 100, spreadsheet.getActiveRange().getNumColumns()).activate();
spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
spreadsheet.getRange('A1:J98').activate();
spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Lids History'), true);
spreadsheet.getRange('A1').activate();
spreadsheet.getRange('\'Current Lid\'!A1:J98').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_NORMAL, false);
spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
};

function a() {
  var spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.getRange('A6:C7').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Lids History'), true);
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
  spreadsheet.getRange('B18').activate();
  spreadsheet.getRange('A6:C7').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Lids'), true);
};

function myFunction1() {
  var spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.getRange('C16').activate();
};

function saveBasicInfo() {
  var spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.getRange('A1:J1').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
  spreadsheet.getRange('A9:J9').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Lids History'), true);
  spreadsheet.getRange('2:2').activate();
  spreadsheet.getActiveSheet().insertRowsBefore(spreadsheet.getActiveRange().getRow(), 1);
  spreadsheet.getActiveRange().offset(0, 0, 1, spreadsheet.getActiveRange().getNumColumns()).activate();
  spreadsheet.getRange('A2').activate();
  spreadsheet.getRange('\'Current Lid\'!A9:J9').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_NORMAL, false);
};

function saveBasicData() {
  var spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.getRange('G12').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
  spreadsheet.getRange('A9:H9').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Lids History'), true);
  spreadsheet.getRange('15:15').activate();
  spreadsheet.setCurrentCell(spreadsheet.getRange('I15'));
  spreadsheet.getActiveSheet().insertRowsBefore(spreadsheet.getActiveRange().getRow(), 1);
  spreadsheet.getActiveRange().offset(0, 0, 1, spreadsheet.getActiveRange().getNumColumns()).activate();
  spreadsheet.getRange('A15').activate();
  spreadsheet.getRange('\'Current Lid\'!A9:H9').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
};

function save_newLine_stage1() {
  var spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.getRange('G11').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Lids History'), true);
  spreadsheet.getRange('15:15').activate();
  spreadsheet.getActiveSheet().insertRowsBefore(spreadsheet.getActiveRange().getRow(), 1);
  spreadsheet.getActiveRange().offset(0, 0, 1, spreadsheet.getActiveRange().getNumColumns()).activate();
};

function save_basicInfo_stage2() {
  var spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.getRange('A15').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
  spreadsheet.getRange('A9:I9').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Lids History'), true);
  spreadsheet.getRange('\'Current Lid\'!A9:I9').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
};



function save_saveCallData_stage3() {
  var spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.getRange('B18').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
  spreadsheet.getRange('A12:F12').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Lids History'), true);
  spreadsheet.getRange('J15').activate();
  spreadsheet.getRange('\'Current Lid\'!A12:F12').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
};

function save_callContent_stage4() {
  var spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.getRange('J12').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Functions'), true);
  var currentCell = spreadsheet.getCurrentCell();
  var sheet = spreadsheet.getActiveSheet();
  sheet.getRange(1, 1, sheet.getMaxRows(), sheet.getMaxColumns()).activate();
  currentCell.activateAsCurrentCell();
  spreadsheet.getActiveRangeList().clear({contentsOnly: true, skipFilteredRows: true})
  .clearFormat();
  spreadsheet.getRange('A1').activate();
  spreadsheet.getRange('\'Current Lid\'!A15:J24').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_NORMAL, false);
  spreadsheet.getRange('J12').activate();
  spreadsheet.getCurrentCell().setFormula('=join(char(10),FILTER(A1:A10,A1:A10<>""))');
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Lids History'), true);
  spreadsheet.getRange('P15').activate();
  spreadsheet.getRange('Functions!J12').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
};



function save_recallNeeded_stage5(){
  save_recallNeeded_stage5_1();
  save_recallNeeded_stage5_2();
};

function save_recallNeeded_stage5_1() {
  var spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.getRange('E15').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
  spreadsheet.getRange('A35:J36').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Functions'), true);
  var sheet = spreadsheet.getActiveSheet();
  sheet.getRange(1, 1, sheet.getMaxRows(), sheet.getMaxColumns()).activate();
  spreadsheet.getActiveRangeList().clear({contentsOnly: true, skipFilteredRows: true})
  .clearFormat();
  spreadsheet.getRange('A1').activate();
  spreadsheet.getRange('\'Current Lid\'!A35:J36').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_NORMAL, false);
  spreadsheet.getRange('A1:J2').activate();
  spreadsheet.getActiveRange().breakApart();
  spreadsheet.getRange('E15').activate();
  spreadsheet.getRange('A1').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Lids History'), true);
  spreadsheet.getRange('Q15').activate();
  spreadsheet.getRange('Functions!A1').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
  spreadsheet.getRange('A38:B43').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Functions'), true);
  sheet = spreadsheet.getActiveSheet();
  sheet.getRange(1, 1, sheet.getMaxRows(), sheet.getMaxColumns()).activate();
  spreadsheet.getActiveRangeList().clear({contentsOnly: true, skipFilteredRows: true})
  .clearFormat();
  spreadsheet.getRange('A1').activate();
  spreadsheet.getRange('\'Current Lid\'!A38:B43').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_NORMAL, false);
  spreadsheet.getRange('A1:B6').activate();
  spreadsheet.getActiveRange().breakApart();
  spreadsheet.getRange('Q15').activate();
  spreadsheet.getRange('A1').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Lids History'), true);
  spreadsheet.getRange('R15').activate();
  spreadsheet.getRange('Functions!A1').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
};

function save_recallNeeded_stage5_2() {
  var spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.getRange('R15').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Functions'), true);
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Saved Functions'), true);

  cleanMultilineTextFunction();
  spreadsheet.getRange('A4:N4').activate();
  spreadsheet.getRange('\'Current Lid\'!C38:J43').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
  spreadsheet.getRange('R15').activate();
  spreadsheet.getRange('B2').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Lids History'), true);
  spreadsheet.getRange('S15').activate();
  spreadsheet.getRange('\'Saved Functions\'!B2').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
  spreadsheet.getRange('A45:B47').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Saved Functions'), true);

  cleanMultilineTextFunction();
  spreadsheet.getRange('A4:N4').activate();
  spreadsheet.getRange('\'Current Lid\'!A45:B47').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
  spreadsheet.getRange('S15').activate();
  spreadsheet.getRange('B2').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Lids History'), true);
  spreadsheet.getRange('T15').activate();
  spreadsheet.getRange('\'Saved Functions\'!B2').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
  spreadsheet.getRange('C45:D47').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Saved Functions'), true);

  cleanMultilineTextFunction();
  spreadsheet.getRange('A4:N4').activate();
  spreadsheet.getRange('\'Current Lid\'!C45:D47').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
  spreadsheet.getRange('T15').activate();
  spreadsheet.getRange('B2').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Lids History'), true);
  spreadsheet.getRange('U15').activate();
  spreadsheet.getRange('\'Saved Functions\'!B2').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
  spreadsheet.getRange('E45:F47').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Saved Functions'), true);

  cleanMultilineTextFunction();
  spreadsheet.getRange('A4:N4').activate();
  spreadsheet.getRange('\'Current Lid\'!E45:F47').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
  spreadsheet.getRange('U15').activate();
  spreadsheet.getRange('B2').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Lids History'), true);
  spreadsheet.getRange('V15').activate();
  spreadsheet.getRange('\'Saved Functions\'!B2').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
  spreadsheet.getRange('G45:H47').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Saved Functions'), true);

  cleanMultilineTextFunction();
  spreadsheet.getRange('A4:N4').activate();
  spreadsheet.getRange('\'Current Lid\'!G45:H47').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
  spreadsheet.getRange('V15').activate();
  spreadsheet.getRange('B2').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Lids History'), true);
  spreadsheet.getRange('W15').activate();
  spreadsheet.getRange('\'Saved Functions\'!B2').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
  spreadsheet.getRange('I45:J47').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Saved Functions'), true);

  cleanMultilineTextFunction();
  spreadsheet.getRange('A4:N4').activate();
  spreadsheet.getRange('\'Current Lid\'!I45:J47').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
  spreadsheet.getRange('W15').activate();
  spreadsheet.getRange('B2').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Lids History'), true);
  spreadsheet.getRange('X15').activate();
  spreadsheet.getRange('\'Saved Functions\'!B2').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);

  cleanMultilineTextFunction();
};

function cleanMultilineTextFunction() {
  var spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Saved Functions'), true);
  spreadsheet.getRange('A4:N21').activate();
  spreadsheet.getActiveRangeList().clear({contentsOnly: true, skipFilteredRows: true});
};

function save_courseSell_stage5() {
  var spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.getRange('Y15').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Functions'), true);
  var sheet = spreadsheet.getActiveSheet();
  sheet.getRange(1, 1, sheet.getMaxRows(), sheet.getMaxColumns()).activate();
  spreadsheet.getActiveRangeList().clear({contentsOnly: true, skipFilteredRows: true})
  .clearFormat();
  spreadsheet.getRange('A1').activate();
  spreadsheet.getCurrentCell().setFormula('=\'Current Lid\'!A35');
  spreadsheet.getRange('Y15').activate();
  spreadsheet.getRange('A1').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Lids History'), true);
  spreadsheet.getRange('Q15').activate();
  spreadsheet.getRange('Functions!A1').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Functions'), true);
  spreadsheet.getRange('A2').activate();
  spreadsheet.getCurrentCell().setFormula('=\'Current Lid\'!A38');
  spreadsheet.getRange('B2').activate();
  spreadsheet.getCurrentCell().setFormula('=\'Current Lid\'!A35')
  .setFormula('=\'Current Lid\'!F38');
  spreadsheet.getRange('Q15').activate();
  spreadsheet.getRange('A2:B2').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Lids History'), true);
  spreadsheet.getRange('Y15').activate();
  spreadsheet.getRange('Functions!A2:B2').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Functions'), true);
  spreadsheet.getRange('A3').activate();
  spreadsheet.getCurrentCell().setFormula('=\'Current Lid\'!A42');
  spreadsheet.getRange('Y15:Z15').activate();
  spreadsheet.getRange('A3').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Lids History'), true);
  spreadsheet.getRange('AA15').activate();
  spreadsheet.getRange('Functions!A3').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
  spreadsheet.getRange('C42:J47').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Saved Functions'), true);
  spreadsheet.getActiveRangeList().clear({contentsOnly: true, skipFilteredRows: true});
  spreadsheet.getRange('A4:N4').activate();
  spreadsheet.getRange('\'Current Lid\'!C42:J47').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
  spreadsheet.getRange('AA15').activate();
  spreadsheet.getRange('B2').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Lids History'), true);
  spreadsheet.getRange('AB15').activate();
  spreadsheet.getRange('\'Saved Functions\'!B2').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
};


function showDialog(msg) {
  var ui = SpreadsheetApp.getUi();
  // Display a modal dialog box with custom HtmlService content.
  var htmlOutput = HtmlService
    .createHtmlOutput('<img src=https://i.stack.imgur.com/AuqJU.gif>');
  ui.showModalDialog(htmlOutput, msg);
}

function closeDialog(msg){
  var html = HtmlService.createHtmlOutput("<script>google.script.host.close();</script>");
  SpreadsheetApp.getUi().showModalDialog(html, msg);

}

function saveLidCall(){
  showDialog('שומר את הליד... אנא המתן');
  var spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.getRange('E12').activate();
  spreadsheet.getRange('\'Saved Functions\'!A26:B26').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);

  save_newLine_stage1();
  save_basicInfo_stage2();
  save_saveCallData_stage3();
  save_callContent_stage4();

  //if "שיחה חוזרת"  
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
  let callStatus = spreadsheet.getRange('A35:J36').getValue();
  if(callStatus === "סטטוס המכירה: מתעניין, צריך שיחה נוספת"){
    save_recallNeeded_stage5();
  }
  else if(callStatus === "סטטוס המכירה: מכירה בוצעה!"){
    save_courseSell_stage5();
  }
  
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true); 

  closeDialog('נשמר');
 
}

function LoadLid() {
  showDialog('טוען ליד... אנא המתן');

  var spreadsheet = SpreadsheetApp.getActive();
  
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Lids'), true);
  var source_sheet = spreadsheet.getSheetByName('Lids');
  var source_range = source_sheet.getActiveRange();
  if(source_range.getValues()[0].length != 6 ){
    closeDialog('טעינה נכשלה! אנא בחר ליד בעמודות A עד F');
    return;
  }
  
  
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Current Lid'), true);
  spreadsheet.getRange('A9:F9').activate();
  source_range.copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);

  spreadsheet.getRange('A6:C7').activate();
  spreadsheet.getRange('B9').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);

  spreadsheet.getRange('C12').activate();
  spreadsheet.getRange('\'Saved Functions\'!A26:B26').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);

  closeDialog('הליד נטען בהצלחה!');

};


/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/*************************      S A V E   H O U R S          ********************/


function SaveHours() {
  showDialog('שומר דיווח שעות, אנא המתן...');

  var spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.getRange('A15:J15').activate();
  spreadsheet.getRange('A6:J6').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Hours History'), true);
  spreadsheet.getRange('14:14').activate();
  spreadsheet.getActiveSheet().insertRowsAfter(spreadsheet.getActiveRange().getLastRow(), 1);
  spreadsheet.getActiveRange().offset(spreadsheet.getActiveRange().getNumRows(), 0, 1, spreadsheet.getActiveRange().getNumColumns()).activate();
  spreadsheet.getActiveRangeList().clear({contentsOnly: true, skipFilteredRows: true})
  .clearFormat()
  .setWrapStrategy(SpreadsheetApp.WrapStrategy.WRAP)
  .setVerticalAlignment('top')
  .setTextDirection(SpreadsheetApp.TextDirection.RIGHT_TO_LEFT);
  spreadsheet.getRange('A15').activate();
  spreadsheet.getRange('Hours!A6:J6').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
  spreadsheet.getRange('C15').activate();
  spreadsheet.getActiveRangeList().setNumberFormat('dd/MM/yyyy');
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Hours'), true);
  spreadsheet.getRange('A6').activate();

  closeDialog('השעות נשמרו בהצלחה!');

};

/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/*************************            L O G I N              ********************/
let maxRow = 0;
function login(){
  showDialog('טוען את השכר שלך, אנא המתן!');

  deleteAllSalaryOutput();
  
  updateMonths();

  let startRow = 19;
  let maxMonths = 15;
  for(let row = startRow; row <= startRow + maxMonths; row++){
    if(!updateHoursForMonth('' + row)){
      maxRow = row - 1;
      break;
    }
  }

  updateSalary(maxRow);

  updateFormats();

  closeDialog('השכר נטען בהצלחה!');
}


function updateMonths() {
  var spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.getRange('A16').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('MySalary'), true);
  spreadsheet.getRange('B6:C6').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Filters'), true);
  spreadsheet.getRange('D7').activate();
  spreadsheet.getRange('MySalary!B6:C6').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
  spreadsheet.getRange('Q16').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Saved Functions'), true);
  spreadsheet.getRange('B31:C31').activate();
  spreadsheet.getRange('Filters!Q16').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Filters'), true);
  spreadsheet.getRange('R16').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Saved Functions'), true);
  spreadsheet.getRange('B32:C32').activate();
  spreadsheet.getRange('Filters!R16').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
  spreadsheet.getRange('B33:B48').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('MySalary'), true);
  spreadsheet.getRange('B19').activate();
  spreadsheet.getRange('\'Saved Functions\'!B33:B48').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
};

function updateHoursForMonth(cellOfMonthRow) {
  var spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('MySalary'), true);
  if(spreadsheet.getRange('B' + cellOfMonthRow).getValue() != ''){
    spreadsheet.getRange('B' + cellOfMonthRow).activate();
    spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Filters'), true);
    spreadsheet.getRange('D8').activate();
    spreadsheet.getRange('MySalary!B'+cellOfMonthRow).copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
    spreadsheet.getActiveRangeList().setNumberFormat('dd/MM/yyyy');
    spreadsheet.getRange('F16:F1000').activate();
    spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Functions'), true);
    var sheet = spreadsheet.getActiveSheet();
    sheet.getRange(1, 1, sheet.getMaxRows(), 3/* max cols*/).activate();
    spreadsheet.getActiveRangeList().clear({contentsOnly: true, skipFilteredRows: true})
    .clearFormat();
    spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Functions'), true);
    spreadsheet.getRange('A1').activate();
    spreadsheet.getRange('Filters!F16:F1000').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_NORMAL, false);
    Utilities.sleep(1 * 1000);
    spreadsheet.getRange('B1').activate();
    spreadsheet.getCurrentCell().setFormula('=SUM(A1:A)');
    spreadsheet.setActiveSheet(spreadsheet.getSheetByName('MySalary'), true);
    spreadsheet.getRange('D' + cellOfMonthRow).activate();
    spreadsheet.getRange('Functions!B1').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
    return true;
  }else{
    return false;
  }
};

function updateSalary(maxRow) {
  var spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.getRange('F13').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('Filters'), true);
  spreadsheet.getRange('S16').activate();
  spreadsheet.setActiveSheet(spreadsheet.getSheetByName('MySalary'), true);
  spreadsheet.getRange('E19:E' + maxRow).activate();
  spreadsheet.getRange('Filters!S16').copyTo(spreadsheet.getActiveRange(), SpreadsheetApp.CopyPasteType.PASTE_VALUES, false);
};


function deleteAllSalaryOutput() {
  var spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.getRange('A19:I34').activate();
  spreadsheet.getActiveRangeList().clear({contentsOnly: true, skipFilteredRows: true});
};


function updateFormats() {
  var spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.getRange('B19:B' + maxRow).activate();
  spreadsheet.getActiveRangeList().setNumberFormat('dd/MM/yyyy');
  spreadsheet.getRange('C19').activate();
  spreadsheet.getCurrentCell().setValue('')
  .setFormula('=F19+H19');
  spreadsheet.getActiveRange().autoFill(spreadsheet.getRange('C19:C' + maxRow), SpreadsheetApp.AutoFillSeries.DEFAULT_SERIES);
  spreadsheet.getRange('D19:D' + maxRow).activate();
  spreadsheet.getActiveRangeList().setNumberFormat('[h]:mm:ss');
  spreadsheet.getRange('E19:E' + maxRow).activate();
  spreadsheet.getActiveRangeList().setNumberFormat('[$₪-40D]#,##0.00');
  spreadsheet.getRange('F19').activate();
  spreadsheet.getCurrentCell().setValue('')
  .setFormula('=E19*24*D19');
  spreadsheet.getActiveRange().autoFill(spreadsheet.getRange('F19:F' + maxRow), SpreadsheetApp.AutoFillSeries.DEFAULT_SERIES);
  spreadsheet.getRange('H19:H' + maxRow).activate();
  spreadsheet.getActiveRangeList().setNumberFormat('[$₪-40D]#,##0.00');
};



/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/********************************************************************************/
/*************************            W H A T S A P P        ********************/

function genWhatsappLink(phonenumber, message){
    // fix phone number
    phonenumber = fixPhonenNumberForWhatsapp(phonenumber);
    // Set WhatsApp link
    var url = 'https://wa.me/' + phonenumber;
    if (message !== ''){
      url += '?text=' + encodeURIComponent(message);
    } 

    return url;
}


function fixPhonenNumberForWhatsapp(phonenumber){
  phonenumber = phonenumber.toString();
  if(phonenumber[0] === '0'){
    let isFirstChar = true;
    let acc = "972";
    for(let i = 0; i < phonenumber.length; i++){
      if(isFirstChar && phonenumber[i] === '0'){
        isFirstChar = false;
        acc = acc + '';
      }else{
        acc = acc + phonenumber[i];
      }
    }
    return acc;

  } else if(phonenumber[0] === '+'){
    return  phonenumber;
  }
  else if(phonenumber.substring(0,3) === '972'){
    return  phonenumber;
  } 
  else if(phonenumber[0] !== '0' && phonenumber[0] !== '9') {
    return '972' + phonenumber;
  }
  return "PHONE ERROR!"
}
